import { TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';

describe('AppComponent', function () {
  beforeEach(() => {
    const formBuilder: FormBuilder = new FormBuilder();

    let store: any = {};

    const mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      },
      removeItem: (key: string) => {
        delete store[key];
      },
      clear: () => {
        store = {};
      }
    };
    spyOn(localStorage, 'getItem').and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, 'setItem').and.callFake(mockLocalStorage.setItem);

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatCardModule,
        FormsModule,
        ReactiveFormsModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        { provide: FormBuilder, useValue: formBuilder }
      ]
    }).compileComponents();
  });

  it('should not allow empty phone', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const component = fixture.componentInstance;
    component.myFormGroup.controls['phone'].markAsTouched();

    fixture.detectChanges();

    const compiled = fixture.nativeElement;
    const numberOfErrors = compiled.querySelectorAll('.error-message').length;

    expect(numberOfErrors).toBe(1);
  });

  it('should store in local storage when data valid', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const component = fixture.componentInstance;

    const firstName = 'first name';
    const lastName = 'last name';
    const email = 'email@email.xyz';
    const phone = '555444333';

    component.myFormGroup.setValue({
      firstName: firstName,
      lastName: lastName,
      email: email,
      phone: phone,
    })

    component.submit();
    fixture.detectChanges();
    expect(localStorage.getItem('firstName')).toEqual(firstName);
    expect(localStorage.getItem('lastName')).toEqual(lastName);
    expect(localStorage.getItem('email')).toEqual(email);
    expect(localStorage.getItem('phone')).toEqual(phone);
  });


  it('should not store in local storage when data is invalid', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const component = fixture.componentInstance;

    component.myFormGroup.setValue({
      firstName: 'name',
      lastName: '',
      email: 'email@email.xyz',
      phone: '555444333',
    })

    component.submit();
    fixture.detectChanges();
    expect(localStorage.getItem('firstName')).toEqual(null);
    expect(localStorage.getItem('lastName')).toEqual(null);
    expect(localStorage.getItem('email')).toEqual(null);
    expect(localStorage.getItem('phone')).toEqual(null);
  });
});
