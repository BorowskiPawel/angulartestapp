import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {

  submitted = false;
  title = 'test-app';

  constructor(private fb: FormBuilder) {

  }

  myFormGroup = this.fb.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    email: ['', [Validators.email, Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")]],
    phone: ['', [Validators.required, Validators.pattern("[0-9]{9}")]]
  });


  get firstName() {
    return this.myFormGroup.get('firstName') as FormControl;
  }

  get lastName() {
    return this.myFormGroup.get('lastName') as FormControl;
  }

  get email() {
    return this.myFormGroup.get('email') as FormControl;
  }

  get phone() {
    return this.myFormGroup.get('phone') as FormControl;
  }

  submit() {
    if(this.myFormGroup.valid)
    {
      localStorage.setItem('firstName', this.firstName.value);
      localStorage.setItem('lastName', this.lastName.value);
      localStorage.setItem('email', this.email.value);
      localStorage.setItem('phone', this.phone.value);
    }
  }

}
